import React, { FC, useState, createContext, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import { getTodosAPI } from '../api/configAPI';
import { TodoItem } from '../components/Dashboard/types';

type iListContent = {
  currentTodo: TodoItem[] | undefined,
  setTodo: (todoArray: TodoItem[]) => void,
  setUpdateTodo: (c: boolean) => void,
}

export const ListContext = createContext<iListContent | undefined>(undefined!);

export const ListProvider:FC = ({ children }) => {
  const [currentTodo, setTodo] = useState<TodoItem[]>();
  const [updateTodo, setUpdateTodo] = useState(false);

  useEffect(() => {
    getTodosAPI().then((data) => {
      if (data.status === 200) {
        setTodo(data.data);
        setUpdateTodo(false);
      }
    });
  }, [updateTodo]);

  return (
    <ListContext.Provider value={{ currentTodo, setTodo, setUpdateTodo }}>
      {children}
    </ListContext.Provider>
  );
};

ListProvider.propTypes = {
  children: PropTypes.node.isRequired
};

export const useList = () => {
  const context = useContext(ListContext);
  if (context === undefined) {
    throw new Error('useList must be used within a ThemeProvider');
  }
  return context;
};
