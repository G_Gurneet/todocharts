import React, { FC, useState, useEffect, createContext, useContext } from 'react';

interface IThemeContext {
    dark: boolean,
    toggleTheme: () => void;
}

export const ThemeContext = createContext<IThemeContext | undefined>(undefined);

export const ThemeProvider: FC = ({ children }) => {
  const themeState = localStorage.getItem('theme') ? JSON.parse(localStorage.getItem('theme') as string) : true;

  const [dark, setDark] = useState(themeState);

  const toggleTheme = () => {
    setDark(!dark);
  };

  useEffect(() => {
    document.documentElement.className = '';
    dark ? document.documentElement.classList.add('theme-dark') : document.documentElement.classList.add('theme-light');
    localStorage.setItem('theme', dark.toString());
  }, [dark]);

  return (
      <ThemeContext.Provider
        value={{
          dark,
          toggleTheme
        }}
      >
          {children}
      </ThemeContext.Provider>
  );
};

export const useTheme = () => {
  const context = useContext(ThemeContext);
  if (context === undefined) {
    throw new Error('useTheme must be used within a ThemeProvider');
  }
  return context;
};
