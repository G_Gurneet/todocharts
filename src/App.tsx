import React from 'react';
import './scss/styles.scss';
import './scss/_custom-bootstrap.scss';
import { BrowserRouter } from 'react-router-dom';

import { NavigationBar } from './components/Navbar';
import { ThemeProvider, ListProvider } from './context';
import Routes from './components/Routes';
import { Footer } from './components';

const App = () => {
  return (
      <BrowserRouter basename={process.env.PUBLIC_URL}>
        <ThemeProvider>
          <ListProvider>
            <NavigationBar />
            <Routes />
          </ListProvider>
            <Footer/>
        </ThemeProvider>
      </BrowserRouter>
  );
};

export default App;
