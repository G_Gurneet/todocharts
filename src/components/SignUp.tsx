import React, { useState } from 'react';
import { Button, Col, Container, Form, Row, Alert } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { signUp } from '../api/configAPI';

const SignUp = () => {
  //   const [passwordView, setPasswordView] = useState(false);
  //   const passwordView = false;
  const [contactDetails, setContactDetails] = useState({
    name: '',
    email: '',
    password: ''
  });
  const [showSignUpBanner, setSignUpBanner] = useState(false);
  const [SignUpBannerMessage, setSignUpBannerMessage] = useState('');

  //   const showPassword = () => (passwordView ? 'text' : 'password');

  const signupSubmit = (e) => {
    e.preventDefault();
    signUp(contactDetails)
      .then((response) => {
        if (response.status) {
          setSignUpBannerMessage(response.data);
          setSignUpBanner(true);
        }
      })
      .catch((err) => {
        setSignUpBannerMessage(err.response.data);
        setSignUpBanner(true);
      });
  };

  const Banner = () => {
    setTimeout(() => {
      setSignUpBanner(false);
      window.location.href = '/login';
    }, 10000);

    const value =
      SignUpBannerMessage.message === 'success' ? 'success' : 'danger';

    return <Alert variant={value}>{SignUpBannerMessage.body}</Alert>;
  };

  return (
    <Container fluid>
      <Row>
        <Col xs={2} sm={3} md={4} lg={4} xl={4} />
        <Col xs={8} sm={6} md={4} lg={4} xl={4}>
          <div className="loginContainer">
            {showSignUpBanner ? Banner() : null}
            <h4> Sign Up </h4>

            <Form onSubmit={(e) => signupSubmit(e)}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label> Name </Form.Label>
                <Form.Control
                  type="name"
                  placeholder="Enter your name"
                  onKeyDown={(e) => {
                    if (e.keyCode === 13) {
                      e.preventDefault();
                    }
                  }}
                  onChange={(e) =>
                    setContactDetails({
                      ...contactDetails,
                      name: e.target.value
                    })
                  }
                />
              </Form.Group>

              <Form.Group controlId="formBasicEmail">
                <Form.Label> Email </Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter your email"
                  onKeyDown={(e) => {
                    if (e.keyCode === 13) {
                      e.preventDefault();
                    }
                  }}
                  onChange={(e) =>
                    setContactDetails({
                      ...contactDetails,
                      email: e.target.value
                    })
                  }
                />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label> Password </Form.Label>
                <Form.Control
                  type="password"
                  className="centerText loginForms"
                  placeholder="Password"
                  onKeyDown={(e) => {
                    if (e.keyCode === 13) {
                      e.preventDefault();
                    }
                  }}
                  onChange={(e) =>
                    setContactDetails({
                      ...contactDetails,
                      password: e.target.value
                    })
                  }
                />
              </Form.Group>

              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
            <div className="">
              <Link to="/login"> Already have an account </Link>
            </div>
          </div>
        </Col>
        <Col xs={2} sm={3} md={4} lg={4} xl={4} />
      </Row>
    </Container>
  );
};

export default SignUp;
