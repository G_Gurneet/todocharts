import React, { FC } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { TodoItem } from './TodoItem';
import { deleteTodoItemAPI } from '../../api/configAPI';
import { useList } from 'context';
import { TodoItem as TodoItemProps } from './types';

type TodoListProps = {
  setAlert: (c: string) => void,
}

const TodoList:FC<TodoListProps> = ({ setAlert }) => {
  const { currentTodo, setUpdateTodo } = useList();

  const todayDate = new Date();

  const filterList = (date : string) => {
    let array: TodoItemProps[] = [];
    switch (date) {
      case 'today':
        array = currentTodo?.filter((todo) => {
          const value = (new Date(todo.completionDate)).toLocaleDateString();
          return value === todayDate.toLocaleDateString();
        })!;
        break;
      case 'overdue':
        array = currentTodo?.filter((todo) => {
          const value = new Date(todo.completionDate);
          if (value.toLocaleDateString() !== todayDate.toLocaleDateString()) {
            return value.getTime() / 1000 < todayDate.getTime() / 1000;
          }
          return null;
        })!;
        break;
      case 'upcoming':
        array = currentTodo?.filter((todo) => {
          const value = Math.round(
            new Date(todo.completionDate).getTime() / 1000
          );
          return value > Date.now() / 1000;
        })!;
        break;
      default:
        break;
    }

    const deleteItem = (id: string) => {
      deleteTodoItemAPI({ id })
        .then((response) => {
          if (response.status === 200) {
            setUpdateTodo(true);
            setAlert('Todo Deleted!');
          }
        })
        .catch(() => {
          // console.log(err);
        });
    };

    // filter by status
    array = array.filter((item) => !item.status);

    // Return the Item
    return (
      <ul>
        {array.map((x) => (
          <li key={x._id}>
            <TodoItem item={x} deleteItem={() => deleteItem(x._id)} />
          </li>
        ))}
      </ul>
    );
  };

  return (
    <Container className="todoList-container">
      <Row>
        <div className="todoList-container_today">
          <Col lg={12}>
            <h4 className="todoList_container_title"> Today </h4>
          </Col>
          <Col lg={12}>{filterList('today')}</Col>
        </div>
      </Row>
      <Row>
        <div className="todoList_container_upcoming">
          <Col lg={12}>
            <h4 className="todoList_container_title">Upcoming </h4>
          </Col>
          <Col lg={12}>{filterList('upcoming')}</Col>
        </div>
      </Row>
      <Row>
        <div className="todoList_container_overdue">
          <Col lg={12}>
            <h4 className="todoList_container_title"> Overdue </h4>
          </Col>
          <Col lg={12}>{filterList('overdue')}</Col>
        </div>
      </Row>
    </Container>
  );
};

TodoList.propTypes = {
  setAlert: PropTypes.func.isRequired
};

export default TodoList;
