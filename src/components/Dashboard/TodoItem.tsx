import React, { FC } from 'react';
import { Container, Row, Col, Badge } from 'react-bootstrap';
import PropTypes from 'prop-types';

type TodoType = {
  item: any;
  deleteItem: any;
}

export const TodoItem:FC<TodoType> = ({ item, deleteItem }) => {
  const day = new Date(item.completionDate).getDate();
  let month = new Date(item.completionDate).getMonth();
  const months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  month = months[month];
  const date = `${day}-${month}`;
  return (
    <>
      <Container className="todoitem_container">
        <Row>
          <Col lg={8}>
            <p className="todoItem_text">
              <b> {date} </b> : {item.item}
            </p>
          </Col>
          <Col lg={2}>
            <Badge
              className="todoitem-delete"
              pill
              onClick={() => deleteItem()}
            >
              {' '}
              DELETE{' '}
            </Badge>
          </Col>
          <Col lg={1}>
            <Badge className="todoitem-done" pill>
              {' '}
              Done{' '}
            </Badge>
          </Col>
        </Row>
      </Container>
    </>
  );
};

TodoItem.propTypes = {
  item: PropTypes.shape({
    item: PropTypes.string.isRequired,
    completionDate: PropTypes.string.isRequired
  }).isRequired,
  deleteItem: PropTypes.func.isRequired
};
