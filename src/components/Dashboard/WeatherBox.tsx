import React, { FC, useEffect, useState } from 'react';
import './Dashboard.css';
import {
  Container,
  Row,
  Col,
  Card,
  ListGroup,
  ListGroupItem
} from 'react-bootstrap';
import ldn from '../../images/london-icon.png';
import swiz from '../../images/switzerland-icon.png';
import punj from '../../images/punjab-icon.png';
import cali from '../../images/california-icon.png';

type WeatherProps = {
  location: string,
  data: any[]
}

const WeatherBox: FC <WeatherProps> = ({ location, data }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [currentWeather, setCurrentWeather] = useState<any>(0);
  const [currentWeatherIcon, setCurrentWeatherIcon] = useState('');
  const [currentTemp, setTemperature] = useState(0);
  const [currentLocation, setLocation] = useState(location);

  useEffect(() => {
    const temp = () => {
      if (isLoading) {
        return 'is loading';
      }

      const currentTemperature: number = (currentWeather.main.temp - 273.15);

      const { icon } = currentWeather.weather[0];
      setTemperature(currentTemperature);
      setCurrentWeatherIcon(`http://openweathermap.org/img/wn/${icon}@2x.png`);

      return currentTemp;
    };

    Array.from(data, (value) => {
      if (value.name === currentLocation) {
        setCurrentWeather(value);
        temp();
        setIsLoading(false);
      }
      return null;
    });
  }, [data, isLoading, currentLocation]);

  const updateSearch = (newLocation: string) => {
    setLocation(newLocation);
    setIsLoading(true);
  };

  return (
    <>
      <Card className="weather-container">
        <Card.Header>
          {currentLocation} {currentTemp} &#8451;{' '}
          <img src={currentWeatherIcon} alt="Weather" width="50" height="50" />
        </Card.Header>
        <ListGroup>
          <ListGroupItem className="weather-listbox">
            <Container>
              <Row>
                <Col xs={3}>
                  <img
                    src={ldn}
                    alt="Forest"
                    className="weather_img"
                    onClick={() => {
                      updateSearch('London');
                    }}
                    aria-hidden="true"
                  />
                </Col>
                <Col xs={3}>
                  <img
                    src={swiz}
                    alt="Forest"
                    className="weather_img"
                    onClick={() => {
                      updateSearch('Zurich');
                    }}
                    aria-hidden="true"
                  />
                </Col>
                <Col xs={3}>
                  <img
                    src={punj}
                    alt="Forest"
                    className="weather_img"
                    onClick={() => {
                      updateSearch('Punjab');
                    }}
                    aria-hidden="true"
                  />
                </Col>
                <Col xs={3}>
                  <img
                    src={cali}
                    alt="Forest"
                    className="weather_img"
                    onClick={() => {
                      updateSearch('California');
                    }}
                    aria-hidden="true"
                  />
                </Col>
              </Row>
            </Container>
          </ListGroupItem>
        </ListGroup>
      </Card>
    </>
  );
};

export default WeatherBox;
