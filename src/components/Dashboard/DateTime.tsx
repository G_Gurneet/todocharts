/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Card } from 'react-bootstrap';

const DateTime = () => {
  const [currentDate, setCurrentDate] = useState(new Date());
  const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sept',
    'Oct',
    'Nov',
    'Dec'
  ];

  const tick = () => {
    setCurrentDate(new Date());
  };

  useEffect(() => {
    const timerID = setInterval(() => tick(), 1000);
    return function cleanUp () {
      clearInterval(timerID);
    };
  });

  const day = currentDate.getDate();
  const month = monthNames[currentDate.getMonth()];

  return (
    <div>
      <Card className="date-time_card">
        <Card.Header>
          <span id="date-time_date">{day}</span>
          <span id="date-time_time">
            {month}
            {' : '}
            {currentDate.toLocaleTimeString()}
          </span>
        </Card.Header>
      </Card>
    </div>
  );
};

export default DateTime;
