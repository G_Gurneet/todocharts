/* eslint-disable no-unused-vars */
/** eslint-disable */
import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import {
  checkAuthenticationAPI,
  updateThought,
  updateMood,
  getThought,
  getMood
} from '../../api/configAPI';
import { Home } from '../Home';
import { Updates } from '.';
import { AddTodo } from './AddTodo';
import TodoList from './TodoList';

export const Dashboard = () => {
  const [getAuth, setAuth] = useState(false);
  const [getCurrentThought, setCurrentThought] = useState('');
  const [getCurrentMood, setCurrentMood] = useState('');
  const [currentAlert, setCurrentAlert] = useState<string | null>(null);

  useEffect(() => {
    checkAuthenticationAPI().then((response) => {
      setAuth(response.data.authenticated);
    });

    getThought().then((data) => {
      setCurrentThought(data.data.thought[0].thought);
    });

    getMood().then((data) => {
      setCurrentMood(data.data.mood[0].mood);
    });
  }, [getAuth]);

  const setAlert = (message: string) => {
    setCurrentAlert(message);
  };

  const onSubmitForm = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const mood = { mood: getCurrentMood };
    const thought = { thought: getCurrentThought };
    updateThought(thought);
    updateMood(mood);
    setAlert('Status Updated');
  };

  return getAuth
    ? (
    <div>
      <Row>
        <Col sm={12} md={12} lg={12}>
          <Updates currentAlert={currentAlert} />
        </Col>
      </Row>
      <Container fluid>
        <Row>
          <Col className="d-none d-lg-block" sm={12} md={12} lg={6}>
            <AddTodo setAlert={setAlert} />
            <TodoList setAlert={setAlert} />
          </Col>

          <Col sm={6}>
            <Row>
              <Col sm={11}>
                <div className="updates-container">
                  <Form onSubmit={(e) => onSubmitForm(e)}>
                    <Row>
                      <Col>
                        <Form.Group className="mb-3">
                          <Form.Label>Thoughts</Form.Label>
                          <Form.Control
                            size="sm"
                            type="text"
                            placeholder={getCurrentThought}
                            onChange={(e) => {
                              setCurrentThought(e.target.value);
                            }}
                          />
                        </Form.Group>
                      </Col>
                      <Col>
                        <Form.Group className="mb-3">
                          <Form.Label>Mood</Form.Label>
                          <Form.Control
                            size="sm"
                            type="text"
                            placeholder={getCurrentMood}
                            onChange={(e) => {
                              setCurrentMood(e.target.value);
                            }}
                          />
                        </Form.Group>
                      </Col>
                    </Row>
                    <Col lg={12}>
                      <Button variant="success" type="submit" size="sm">
                        Update
                      </Button>
                    </Col>
                  </Form>
                </div>
              </Col>
              <Col className="d-md-block d-lg-none" sm={0} lg={10}>
                <div className="chart-container">
                  <h4>Charts to Show History of Todo&#39s</h4>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
      )
    : (
    <Home />
      );
};
