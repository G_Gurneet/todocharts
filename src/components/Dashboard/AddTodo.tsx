import React, { FC, useState } from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useList } from 'context';
import { setTodoAPI } from '../../api/configAPI';

interface iAddTodoProps {
  setAlert: (state: string) => void;
}

export const AddTodo:FC <iAddTodoProps> = ({ setAlert }) => {
  const [currentTask, setTask] = useState('');
  const [taskEndDate, setEndDate] = useState<Date | string>(new Date());

  const { currentTodo, setUpdateTodo, setTodo } = useList();

  const onFormSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const AllTodo = currentTodo;

    setTodoAPI({ item: currentTask, date: taskEndDate }).then((data) => {
      if (data.status === 200) {
        AllTodo?.push(data.data);
        setTodo(AllTodo!);
        setUpdateTodo(true);
      }
    });
    setAlert('A new task has been added!');
    setTask('');
    setEndDate(new Date());
  };

  return (
    <div className="addtodo-container">
      <Form onSubmit={(e) => onFormSubmit(e)}>
        <Row>
          <Col sm={10} lg={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="I want to..."
              onChange={(a) => {
                setTask(a.target.value);
              }}
              value={currentTask}
            />
          </Col>
          <Col sm={2} lg={4}>
            <Form.Control
              size="sm"
              type="date"
              onChange={(e) => {
                setEndDate(e.target.value);
              }}
            />
          </Col>
          <Col lg={2}>
            <Button
              data-testid="FormSubmit"
              type="submit"
              value="Submit"
              size="sm"
              block
              disabled={!currentTask.length}
              variant="secondary"
            >
              Submit
            </Button>
          </Col>
        </Row>
      </Form>
    </div>
  );
};

AddTodo.propTypes = {
  setAlert: PropTypes.func.isRequired
};
