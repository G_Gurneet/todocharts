import React, { FC, useEffect, useState } from 'react';
import { Container, Row, Col, Alert } from 'react-bootstrap';
import socketIOClient from 'socket.io-client';
import PropTypes from 'prop-types';

import DateTime from './DateTime';
import WeatherBox from './WeatherBox';
import { serverURL } from '../../config';

type UpdateProps = {
  currentAlert: string | null;
}

export const Updates:FC <UpdateProps> = ({ currentAlert }) => {
  const [weatherData, setWeatherData] = useState([]);
  const [showAlert, setShowAlert] = useState(false);

  useEffect(() => {
    const socket = socketIOClient(serverURL);
    socket.on('weatherData', (msg) => {
      const data = msg.test;
      setWeatherData(data);
    });
  }, []);

  useEffect(() => {
    setInterval(() => {
      setShowAlert(false);
    }, 6000);
  }, [showAlert]);

  useEffect(() => {
    if (currentAlert) {
      setShowAlert(true);
    }
  }, [currentAlert]);

  return (
    <Container fluid className="updates-container">
      <Row>
        <Col xs={12} md={4} lg={3}>
          <WeatherBox location="London" data={weatherData} />
        </Col>
        <Col md={5} className="d-none d-lg-block d-md-block">
          <DateTime />
        </Col>
        <Col md={2}>
          <Alert variant="info" show={showAlert}>
            {currentAlert}
          </Alert>
        </Col>
      </Row>
    </Container>
  );
};

Updates.propTypes = {
  currentAlert: PropTypes.string
};

Updates.defaultProps = {
  currentAlert: null
};
