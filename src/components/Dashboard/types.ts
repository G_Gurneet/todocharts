export type TodoItem = {
    _id: string,
    status: boolean,
    item: string,
    completionDate: Date,
    createdAt: Date,
    updatedAt: Date,
  }
