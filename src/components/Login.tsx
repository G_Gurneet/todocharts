import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { login } from '../api/configAPI';

type AlertMessage = {
  message: string,
  variant: string,
}

const Login = () => {
  const [emailState, setEmailState] = useState('');
  const [passwordState, setPasswordState] = useState('');
  const [isActive, setIsActive] = useState(true);
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState<AlertMessage>({ message: '', variant: '' });

  const loginHandler = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    login({ email: emailState, password: passwordState })
      .then((response) => {
        if (response.status === 200) {
          setAlertMessage({ message: 'Success!', variant: 'success' });
          setShowAlert(true);
          setTimeout(() => {
            setShowAlert(false);
            setPasswordState('');
            window.location.href = '/';
          }, 2000);
        }
      })
      .catch(() => {
        setAlertMessage({
          message: 'Password or Username did not match',
          variant: 'danger'
        });
        setShowAlert(true);
        setTimeout(() => {
          setShowAlert(false);
          setPasswordState('');
        }, 3000);
      });
  };

  useEffect(() => {
    if (emailState && passwordState) {
      setIsActive(false);
    } else {
      setIsActive(true);
    }
  }, [emailState, passwordState]);

  return (
    <Container fluid>
      <Row noGutters>
        <Col
          xs={{ span: 10, offset: 1 }}
          sm={{ span: 8, offset: 1 }}
          md={{ span: 4, offset: 4 }}
        >
          <div className="login-container">
            <h1> Login </h1>
            <Form onSubmit={(e) => loginHandler(e)}>
              <Form.Group controlId="formBasicEmail">
                <Form.Control
                  type="email"
                  placeholder="Email Address"
                  onChange={(e) => setEmailState(e.target.value)}
                />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Control
                  type="password"
                  className="centerText loginForms"
                  placeholder="Password"
                  onChange={(e) => setPasswordState(e.target.value)}
                />
              </Form.Group>
              {showAlert
                ? (
                <Button
                  variant={alertMessage.variant}
                  type="submit"
                  disabled={isActive}
                >
                  {alertMessage.message}
                </Button>
                  )
                : (
                <Button variant="warning" type="submit" disabled={isActive}>
                  Login
                </Button>
                  )}
            </Form>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default Login;
