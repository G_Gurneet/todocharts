import React, { useEffect, useRef } from 'react';
import * as THREE from 'three';
import { useTheme } from 'context';

export const Stack = () => {
  const mountRef = useRef<HTMLDivElement>(null!);

  const { dark } = useTheme();

  useEffect(() => {
    const backgroundColor = dark ? 0x000000 : 0xEAEAE0;

    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(
      75, window.innerWidth / window.innerHeight, 0.1, 1000
    );
    const renderer = new THREE.WebGLRenderer();
    renderer.setClearColor(backgroundColor);
    renderer.setSize(window.innerWidth, window.innerHeight);
    mountRef.current.appendChild(renderer.domElement);

    const geometry = new THREE.BoxGeometry(1, 1, 1);
    const material = new THREE.MeshBasicMaterial({ color: dark ? 0x343a40 : 0x5885AF });
    const cube = new THREE.Mesh(geometry, material);

    scene.add(cube);
    camera.position.z = 5;

    const animate = () => {
      requestAnimationFrame(animate);
      cube.rotation.x += 0.01;
      cube.rotation.y += 0.01;
      renderer.render(scene, camera);
    };

    const onWindowResize = () => {
      camera.aspect = window.innerWidth / window.innerHeight;
      camera.updateProjectionMatrix();
      renderer.setSize(window.innerWidth, window.innerHeight);
    };

    window.addEventListener('resize', onWindowResize, false);

    animate();
  }, [dark]);

  return (
    <div className="stack-container" ref={mountRef} />
  );
};
