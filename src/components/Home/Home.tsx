import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Container, Row, Col, Spinner } from 'react-bootstrap';
import { todoValue, getThought, getMood } from '../../api/configAPI';

type QouteType = {
  quote: string,
  author: string,
}

export const Home = () => {
  const [getQoute, setQoute] = useState({} as QouteType);
  const [getTodoValues, setTodoValues] = useState(0);
  const [getThoughts, setThoughts] = useState('');
  const [getMoods, setMood] = useState('');

  useEffect(() => {
    axios.get('https://quotes.rest/qod?language=en').then((data) => {
      setQoute(data.data.contents.quotes[0]);
    });

    todoValue().then((response) => {
      if (response.status === 200) {
        setTodoValues(response.data.todoValue);
      }
    });

    getThought().then((response) => {
      setThoughts(response.data.thought[0].thought);
    });

    getMood().then((response) => {
      setMood(response.data.mood[0].mood);
    });
  }, [getTodoValues]);

  const loadingSpinner = () => (
    <div>
      <Spinner animation="grow" variant="secondary" />
      <Spinner animation="grow" variant="secondary" />
      <Spinner animation="grow" variant="secondary" />
    </div>
  );

  return (
    <Container>
      <Row>
        <Col className="home-col" sm={12} md={12} lg={12}>
          <div>
            <h1 className="home-title"> Hey , I&apos;m Gurneet</h1>
            <p className="home-description">
              I write code when I&apos;m free.
            </p>
          </div>
        </Col>
      </Row>

      <Row className="home-stats">
        <Col className="home-col" sm={12} md={4} lg={3}>
          <div className="home-stats_container">
            <h6> Pending Todos </h6>
            {!getTodoValues
              ? (
                  loadingSpinner()
                )
              : (
              <p className="home-white home-value">{getTodoValues}</p>
                )}
          </div>
        </Col>

        <Col className="home_col" sm={12} md={4} lg={6}>
          <div className="home-stats_container">
            {!getQoute
              ? (
                  loadingSpinner()
                )
              : (
              <p className="home-white">{getQoute.quote}</p>
                )}

            <p>
              Author:
              {getQoute.author}
            </p>
          </div>
        </Col>

        <Col className="home_col" sm={12} md={4} lg={3}>
          <div className="home-stats_container">
            <h6> Current Mood </h6>
            {!getMoods
              ? (
                  loadingSpinner()
                )
              : (
              <p className="home-white home-mood">{getMoods}</p>
                )}
          </div>
        </Col>
      </Row>

      <Row className="home-stats">
        <Col className="home_col" sm={12} md={6} lg={6}>
          <div className="home-stats_container">
          <h6> Spotify Link </h6>
          {loadingSpinner()}
          </div>
        </Col>
        <Col className="home_col" sm={12} md={6} lg={6}>
          <div className="home-stats_container">
            <h6> Thoughts </h6>
            {!getThoughts
              ? (
                  loadingSpinner()
                )
              : (
              <p className="home-white">{getThoughts}</p>
                )}
          </div>
        </Col>
      </Row>

      <Row className="home-stats">
        <Col className="home_col" sm={12} md={12} lg={12}>
          <h3> Projects </h3>
          <p> Still to add ..</p>
        </Col>
      </Row>
    </Container>
  );
};
