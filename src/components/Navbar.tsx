import React, { useEffect, useState } from 'react';
import { Navbar, Nav, Container, Row, Col } from 'react-bootstrap';
import { checkAuthenticationAPI, logout } from '../api/configAPI';
import { useTheme } from 'context';

export const NavigationBar = () => {
  const [getAuth, setAuth] = useState(false);

  const { dark, toggleTheme } = useTheme();

  useEffect(() => {
    checkAuthenticationAPI().then((response) => {
      setAuth(response.data.authenticated);
    });
  }, [getAuth]);

  const logoutHandler = () => {
    logout().then((response) => {
      if (response.status === 200) {
        setAuth(false);
      }
    });
  };

  return getAuth
    ? (
    <Container>
      <Row className="justify-content-md-center">
        <Col xs={12} lg={6}>
          <Navbar
            variant="dark"
            expand="md"
            sticky="top"
          >
            <Nav.Link href="/home" className="ml-auto navbar-font">
              Home
            </Nav.Link>
            <Nav.Link href="/" className="ml-auto navbar-font">
              Dashboard
            </Nav.Link>
            <Nav.Link href="/blog" className="ml-auto navbar-font">
              Blog
            </Nav.Link>
            <Nav.Link href="/stack" className="ml-auto navbar-font">
              Stack
            </Nav.Link>
            <Nav.Link onClick={logoutHandler} className="ml-auto navbar-font ">
              Logout
            </Nav.Link>
            <span onClick={() => toggleTheme()} className="navbar-toggle">
              {dark ? <> &#127773; </> : <> &#127770; </> }
            </span>
          </Navbar>
        </Col>
      </Row>
    </Container>
      )
    : (
    <Container fluid className="p-0">
      <Row noGutters>
        <Col md={4} className='navbar-col'/>
        <Col xs={12} md={{ span: 4 }}>
          <Navbar variant={dark ? 'dark' : 'light' } bg={dark ? 'dark' : 'light' } expand="lg">
            <Nav.Link href="/home" className="navbar-font">
              Home
            </Nav.Link>
            <Nav.Link href="/blog" className="ml-auto navbar-font">
              Blog
            </Nav.Link>
            <Nav.Link href="/stack" className="ml-auto navbar-font">
              Stack
            </Nav.Link>
            <span onClick={() => toggleTheme()} className="navbar-toggle">
              {dark ? <> &#127773; </> : <> &#127770; </> }
            </span>
          </Navbar>
        </Col>
        <Col md={4} className='navbar-col'/>
      </Row>
    </Container>
      );
};
