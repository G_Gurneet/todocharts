import React from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import Login from './Login';
// import SignUp from './SignUp';
import { Home } from './Home';
import { Stack } from './Stack';
import { Blog } from './Blog';
import { Dashboard } from './Dashboard';

const Routes = () => (
  <BrowserRouter basename={process.env.PUBLIC_URL}>
    <Switch>
      <Route exact path="/">
        <Dashboard />
      </Route>
      <Route exact path="/login">
        <Login />
      </Route>
      {/* <Route exact path="/signup">
        <SignUp />
      </Route> */}
      <Route exact path="/home">
        <Home />
      </Route>
      <Route exact path="/blog">
        <Blog />
      </Route>
      <Route exact path="/stack">
        <Stack />
      </Route>
    </Switch>
  </BrowserRouter>
);

export default Routes;
