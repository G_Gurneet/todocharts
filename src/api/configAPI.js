import axios from 'axios';
import { serverURL } from '../config';

export const setTodoAPI = async (todo) => {
  const response = await axios.post(`${serverURL}/api/todo/todoitem`, todo, { withCredentials: true });
  return response;
};

export const getTodosAPI = async () => {
  const response = await axios.get(`${serverURL}/api/todo/todoitems`, { withCredentials: true });
  return response;
};

export const deleteTodoItemAPI = async (id) => {
  const response = await axios.post(`${serverURL}/api/todo/deleteTodoItems`, id, { withCredentials: true });
  return response;
};

export const signUp = async (body) => {
  const response = await axios.post(`${serverURL}/api/todo/signUp`, body);
  return response;
};

export const login = async (body) => {
  const response = await axios.post(`${serverURL}/api/todo/login`, body, { withCredentials: true });
  return response;
};

export const logout = async () => {
  const response = await axios.get(`${serverURL}/api/todo/logout`, { withCredentials: true });
  return response;
};

export const checkAuthenticationAPI = async () => {
  const response = await axios.get(`${serverURL}/api/todo/checkAuthentication`, { withCredentials: true });
  return response;
};

export const todoValue = async () => {
  const response = await axios.get(`${serverURL}/api/todos`);
  return response;
};

export const updateThought = async (body) => {
  const response = await axios.post(`${serverURL}/api/todo/thoughts`, body, { withCredentials: true });
  return response;
};

export const getThought = async () => {
  const response = await axios.get(`${serverURL}/api/thoughts`);
  return response;
};

export const getMood = async () => {
  const response = await axios.get(`${serverURL}/api/moods`);
  return response;
};

export const updateMood = async (body) => {
  const response = await axios.post(`${serverURL}/api/todo/mood`, body, { withCredentials: true });
  return response;
};
